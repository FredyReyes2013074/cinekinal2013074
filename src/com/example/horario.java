package com.example;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/horario")
public class horario extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String titulo;
	private String horario;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		titulo=request.getParameter("titulo");
		horario=request.getParameter("horario");
		request.setAttribute("titulo", titulo);
		request.setAttribute("horario", horario);
		request.getRequestDispatcher("peliculas/compras/compra1.jsp").forward(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
