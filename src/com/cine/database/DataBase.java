package com.cine.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class DataBase {
	private static final DataBase INSTANCE = new DataBase();
	
	private Connection conexion;
	
	private Statement stmt;
	
	public static DataBase getInstance(){
		return INSTANCE;
	}
	
	private DataBase(){
		try{
			Class.forName("oracle.jdbc.OracleDriver").newInstance();
			conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","Fre","test123");
			stmt=conexion.createStatement();
		}catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | SQLException e){
			e.printStackTrace();
		}
	}
	
	public void ejecutarConsulta(String consulta){
		try{
			stmt.execute(consulta);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	
	public ResultSet obtenerConsulta(String consulta){
		ResultSet result= null;
		try{
			result= stmt.executeQuery(consulta);
		}catch (SQLException e){
			e.printStackTrace();
		}
		return result;
	}
	
}
