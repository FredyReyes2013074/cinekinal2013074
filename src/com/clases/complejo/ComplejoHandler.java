package com.clases.complejo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.cine.database.DataBase;
import com.clases.complejo.Complejo;

public class ComplejoHandler {
private static final ComplejoHandler INSTANCE = new ComplejoHandler();
	
	public static final ComplejoHandler getInstance(){
		return INSTANCE;
	}
	
	public Complejo getComplejoById(Integer id){
		
		Complejo com = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Complejo WHERE idComplejo = " + id);
		
		try {
			while(result.next()){
				com = new Complejo(result.getInt("idComplejo"), result.getString("nombre"),result.getString("direccion"),result.getString("cuidad"),result.getString("callCenter"),result.getString("latitud"),result.getString("longitud"),result.getString("mapa"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return com;
	}
	
	public ArrayList<Complejo> getComplejoList(){
		
		ArrayList<Complejo> list = new ArrayList<Complejo>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Complejo");
		
		try {
			while(result.next()){
				list.add(new Complejo(result.getInt("idComplejo"), result.getString("nombre"),result.getString("direccion"),result.getString("cuidad"),result.getString("callCenter"),result.getString("latitud"),result.getString("longitud"),result.getString("mapa")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
}
