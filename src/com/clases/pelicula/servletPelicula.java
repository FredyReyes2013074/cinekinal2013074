package com.clases.pelicula;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clases.pelicula.PeliculaHandler;


@WebServlet("/servletPelicula")
public class servletPelicula extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("idPelicula") != null){
			
			Integer idp = Integer.parseInt(request.getParameter("idPelicula"));
			
			request.setAttribute("pelicula", PeliculaHandler.getInstance().getPeliculaById(idp));
			
			request.getRequestDispatcher("/peliculas/peli1.jsp").forward(request, response);
		}
			
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
