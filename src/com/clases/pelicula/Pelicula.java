package com.clases.pelicula;

public class Pelicula {
	private Integer idPelicula;
	private String nombre;
	private String descripcion;
	private String trailer;
	private String duracion;
	private String imagen;
	private String imagen2;
	private Integer idClasificacion;
	public Integer getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(Integer idPelicula) {
		this.idPelicula = idPelicula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getTrailer() {
		return trailer;
	}
	public void setTrailer(String trailer) {
		this.trailer = trailer;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getImagen2() {
		return imagen2;
	}
	public void setImagen2(String imagen2) {
		this.imagen2 = imagen2;
	}
	public Integer getIdClasificacion() {
		return idClasificacion;
	}
	public void setIdClasificacion(Integer idClasificacion) {
		this.idClasificacion = idClasificacion;
	}
	public Pelicula(Integer idPelicula, String nombre, String descripcion,
			String trailer, String duracion, String imagen, String imagen2,
			Integer idClasificacion) {
		super();
		this.idPelicula = idPelicula;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.trailer = trailer;
		this.duracion = duracion;
		this.imagen = imagen;
		this.imagen2 = imagen2;
		this.idClasificacion = idClasificacion;
	}
	public Pelicula() {
		super();
	}
	
	
	
	
}
