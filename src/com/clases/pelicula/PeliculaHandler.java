package com.clases.pelicula;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.cine.database.DataBase;
import com.clases.pelicula.Pelicula;

public class PeliculaHandler {
private static final PeliculaHandler INSTANCE = new PeliculaHandler();
	
	public static final PeliculaHandler getInstance(){
		return INSTANCE;
	}
	
	public Pelicula getPeliculaById(Integer id){
		
		Pelicula peli = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Pelicula WHERE idPelicula = " + id);
		
		try {
			while(result.next()){
				peli = new Pelicula(result.getInt("idPelicula"), result.getString("nombre"),result.getString("descripcion"),result.getString("trailer"),result.getString("duracion"),result.getString("imagen"),result.getString("imagen2"),result.getInt("idClasificacion"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return peli;
	}
	
	public ArrayList<Pelicula> getPeliculaList(){
		
		ArrayList<Pelicula> list = new ArrayList<Pelicula>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Pelicula");
		
		try {
			while(result.next()){
				list.add(new Pelicula(result.getInt("idPelicula"), result.getString("nombre"), result.getString("descripcion"),result.getString("trailer"),result.getString("duracion"),result.getString("imagen"),result.getString("imagen2"),result.getInt("idClasificacion")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
}
