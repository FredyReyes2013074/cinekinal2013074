package com.clases.categoria;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.cine.database.DataBase;
import com.clases.categoria.Categoria;

public class CategoriaHandler {
	
private static final CategoriaHandler INSTANCE = new CategoriaHandler();
	
	public static final CategoriaHandler getInstance(){
		return INSTANCE;
	}
	
	public Categoria getCategoriaById(Integer id){
		
		Categoria cat = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Categoria WHERE idCategoria = " + id);
		
		try {
			while(result.next()){
				cat = new Categoria(result.getInt("idCategoria"), result.getString("nombre"),result.getString("descripcion"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cat;
	}
	
	public ArrayList<Categoria> getCategoriaList(){
		
		ArrayList<Categoria> list = new ArrayList<Categoria>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Categoria");
		
		try {
			while(result.next()){
				list.add(new Categoria(result.getInt("idCategoria"), result.getString("nombre"),result.getString("descripcion")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
}
