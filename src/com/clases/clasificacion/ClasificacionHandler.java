package com.clases.clasificacion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.cine.database.DataBase;

public class ClasificacionHandler {
	private static final ClasificacionHandler INSTANCE = new ClasificacionHandler();
	
	public static final ClasificacionHandler getInstance(){
		return INSTANCE;
	}
	
	public Clasificacion getClasificacionById(Integer id){
		
		Clasificacion cla = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Clasificacion WHERE idClasficacion = " + id);
		
		try {
			while(result.next()){
				cla = new Clasificacion(result.getInt("idClasficacion"), result.getString("nombre"), result.getString("descripcion"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cla;
	}
	
	public ArrayList<Clasificacion> getClasificacionList(){
		
		ArrayList<Clasificacion> list = new ArrayList<Clasificacion>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Clasificacion");
		
		try {
			while(result.next()){
				list.add(new Clasificacion(result.getInt("idClasificacion"), result.getString("nombre"), result.getString("descripcion")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
}
