package com.clases.clasificacion;

public class Clasificacion {
	private Integer idClasificacion;
	private String  nombre;
	private String 	descripcion;
	public Integer getIdClasificacion() {
		return idClasificacion;
	}
	public void setIdClasificacion(Integer idClasificacion) {
		this.idClasificacion = idClasificacion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Clasificacion(Integer idClasificacion, String nombre,
			String descripcion) {
		super();
		this.idClasificacion = idClasificacion;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	public Clasificacion() {
		super();
		
	}
	
	
}
