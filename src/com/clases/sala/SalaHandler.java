package com.clases.sala;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.cine.database.DataBase;


public class SalaHandler {
	
	private static final SalaHandler INSTANCE = new SalaHandler();
	
	public static final SalaHandler getInstance(){
		return INSTANCE;
	}
	
	public Sala getSalaById(Integer id){
		
		Sala sal = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Sala WHERE idSala = " + id);
		
		try {
			while(result.next()){
				sal = new Sala(result.getInt("idComplejo"),result.getString("nombre"),result.getString("tipo"),result.getInt("idComplejo"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sal;
	}
	
	public ArrayList<Sala> getSalaList(){
		
		ArrayList<Sala> list = new ArrayList<Sala>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Sala");
		
		try {
			while(result.next()){
				list.add(new Sala(result.getInt("idComplejo"),result.getString("nombre"),result.getString("tipo"),result.getInt("idComplejo")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
}
