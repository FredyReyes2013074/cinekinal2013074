package com.clases.cartelera;

public class Cartelera {
	private Integer idCartelera;
	private String nombre;
	private String horaIncio;
	private String horaFin;
	private String fecha;
	private Integer idSala;
	private Integer idPelicula;
	public Integer getIdCartelera() {
		return idCartelera;
	}
	public void setIdCartelera(Integer idCartelera) {
		this.idCartelera = idCartelera;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getHoraIncio() {
		return horaIncio;
	}
	public void setHoraIncio(String horaIncio) {
		this.horaIncio = horaIncio;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Integer getIdSala() {
		return idSala;
	}
	public void setIdSala(Integer idSala) {
		this.idSala = idSala;
	}
	public Integer getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(Integer idPelicula) {
		this.idPelicula = idPelicula;
	}
	public Cartelera(Integer idCartelera, String nombre, String horaIncio,
			String horaFin, String fecha, Integer idSala, Integer idPelicula) {
		super();
		this.idCartelera = idCartelera;
		this.nombre = nombre;
		this.horaIncio = horaIncio;
		this.horaFin = horaFin;
		this.fecha = fecha;
		this.idSala = idSala;
		this.idPelicula = idPelicula;
	}
	public Cartelera() {
		super();
	}
	
	
}
