package com.clases.cartelera;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clases.cartelera.CarteleraHandler;


@WebServlet("/servletCartelera")
public class servletCartelera extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("idCartelera") != null){
			
			Integer idcat = Integer.parseInt(request.getParameter("idCartelera"));
			
			request.setAttribute("cartelera", CarteleraHandler.getInstance().getCarteleraById(idcat));
			
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
