package com.clases.cartelera;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.cine.database.DataBase;
import com.clases.cartelera.Cartelera;

public class CarteleraHandler {
	
private static final CarteleraHandler INSTANCE = new CarteleraHandler();
	
	public static final CarteleraHandler getInstance(){
		return INSTANCE;
	}
	
	public Cartelera getCarteleraById(Integer id){
		
		Cartelera cat = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Cartelera WHERE idCartelera = " + id);
		
		try {
			while(result.next()){
				cat = new Cartelera(result.getInt("idCartelera"),result.getString("nombre"),result.getString("horaIncio"),result.getString("horaFin"),result.getString("fecha"),result.getInt("idSala"),result.getInt("idPelicula"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cat;
	}
	
	public ArrayList<Cartelera> getCarteleraList(){
		
		ArrayList<Cartelera> list = new ArrayList<Cartelera>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Pelicula");
		
		try {
			while(result.next()){
				list.add(new Cartelera(result.getInt("idCartelera"),result.getString("nombre"),result.getString("horaIncio"),result.getString("horaFin"),result.getString("fecha"),result.getInt("idSala"),result.getInt("idPelicula")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
}
