<!DOCTYPE html>
<html>
<head>
<title>Complejo</title>
<link rel="stylesheet" type="text/css" href="complejos/styleC.css">
</head>

<body>
	<div id="cabeza">
		<p id="titulo">${complejseleccion.getNombre()}</p>
	</div>
	<div id="contenedoriz">
		<p id="direccion">${complejseleccion.getDireccion()}</p>
		<p id="callcenter">Tel: ${complejseleccion.getCallCenter()}</p>
	</div>
	
	<div id="div_mapa">
		<iframe id="mapa" src="${complejseleccion.getMapa()}"></iframe>
	</div>
</body>

</html>