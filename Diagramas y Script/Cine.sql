---------------------Clasificaqcion--------------------
create table Clasificacion (
  idClasficacion number (10),
  nombre varchar(255),
  descripcion varchar(255),
  CONSTRAINT clasi_pk PRIMARY KEY (idClasficacion)
);

CREATE SEQUENCE clasi_seq;

CREATE OR REPLACE TRIGGER clasi_bir 
BEFORE INSERT ON Clasificacion 
FOR EACH ROW

BEGIN
  SELECT clasi_seq.NEXTVAL
  INTO   :new.idClasficacion
  FROM   dual;
END;



-----------------------Pelicula-------------------
create table Pelicula (
  idPelicula number (10),
  nombre varchar(255),
  descripcion varchar(900),
  trailer varchar(255),
  duracion varchar(255),
  imagen varchar(255),
  imagen2 varchar (255),
  idClasificacion number (10),
  CONSTRAINT pel_pk PRIMARY KEY (idPelicula),
  CONSTRAINT clasi_fk FOREIGN KEY  (idClasificacion) References Clasificacion(idClasficacion)
);


CREATE SEQUENCE pel_seq;

CREATE OR REPLACE TRIGGER pel_bir 
BEFORE INSERT ON Pelicula 
FOR EACH ROW

BEGIN
  SELECT pel_seq.NEXTVAL
  INTO   :new.idPelicula
  FROM   dual;
END;

---------------------Complejo-------------------------
create table Complejo (
  idComplejo number,
  nombre varchar(255),
  direccion varchar(255),
  cuidad varchar(255),
  callCenter varchar(255),
  latitud varchar(255),
  longitud varchar(255),
  mapa   varchar(900),
  CONSTRAINT cop_pk PRIMARY KEY (idComplejo)
);



CREATE SEQUENCE cop_seq;

CREATE OR REPLACE TRIGGER cop_bir 
BEFORE INSERT ON Complejo 
FOR EACH ROW

BEGIN
  SELECT cop_seq.NEXTVAL
  INTO   :new.idcomplejo
  FROM   dual;
END;

-----------------------Sala------------------

create table Sala (
  idSala number (10),
  nombre varchar(255),
  tipo varchar(255),
  idComplejo number (10),
  CONSTRAINT sal_pk PRIMARY KEY (idSala),
  CONSTRAINT cop_fk FOREIGN KEY (idComplejo) References Complejo(idComplejo)
);

CREATE SEQUENCE sal_seq;

CREATE OR REPLACE TRIGGER sal_bir 
BEFORE INSERT ON Sala 
FOR EACH ROW

BEGIN
  SELECT sal_seq.NEXTVAL
  INTO   :new.idSala
  FROM   dual;
END;

-------------------Aientos------------------------

create table Asiento (
  idAsiento number (10),
  nombre varchar2(255),
  idSala number (10),
  CONSTRAINT asi_pk PRIMARY KEY (idAsiento)
);

CREATE SEQUENCE asi_seq;

CREATE OR REPLACE TRIGGER asi_bir 
BEFORE INSERT ON Asiento
FOR EACH ROW

BEGIN
  SELECT asi_seq.NEXTVAL
  INTO   :new.idAsiento
  FROM   dual;
END;


------------------------Carterlera-----------------------------
create table Cartelera (
  idCartelera number(10),
  nombre varchar(255),
  horaIncio varchar2(255),
  horaFin varchar2(255),
  fecha date,
  idSala number (10),
  idPelicula number (10),
  CONSTRAINT car_pk PRIMARY KEY (idCartelera),
  CONSTRAINT sal_fk FOREIGN KEY (idSala) References Sala(idSala),
  CONSTRAINT pel_fk FOREIGN KEY (idPelicula) References Pelicula(idPelicula)
);

CREATE SEQUENCE car_seq;

CREATE OR REPLACE TRIGGER car_bir 
BEFORE INSERT ON Cartelera
FOR EACH ROW

BEGIN
  SELECT car_seq.NEXTVAL
  INTO   :new.idCartelera
  FROM   dual;
END;

------------------Estreno---------------------
create table Estreno (
  idEstreno number(10),
  fecha date,
  idPelicula number (10),
  CONSTRAINT est_pk PRIMARY KEY (idEstreno),
  CONSTRAINT cat_fk FOREIGN KEY (idPelicula) References Pelicula(idPelicula)
);

CREATE SEQUENCE est_seq;

CREATE OR REPLACE TRIGGER est_bir 
BEFORE INSERT ON Estreno
FOR EACH ROW

BEGIN
  SELECT est_seq.NEXTVAL
  INTO   :new.idEstreno
  FROM   dual;
END;

--------------------Categoria------------------------------
create table Categoria(
  idCategoria number (10),
  nombre varchar(255),
  descripcion varchar(255),
  CONSTRAINT cat_pk PRIMARY KEY (idCategoria)
);

CREATE SEQUENCE cat_seq;

CREATE OR REPLACE TRIGGER cat_bir 
BEFORE INSERT ON Categoria
FOR EACH ROW

BEGIN
  SELECT cat_seq.NEXTVAL
  INTO   :new.idCategoria
  FROM   dual;
END;

--------------Pelicula_Categoria----------------
create table Pelicula_Categoria (
  idPelicula number (10),
  idCategoria number (10),
  CONSTRAINT peli_fk FOREIGN KEY (idPelicula) References Pelicula(idPelicula),
  CONSTRAINT cate_fk FOREIGN KEY (idCategoria) References Categoria(idCategoria)
);



--------------Funcion-------------------
create table Funcion (
  idFuncion number (10),
  nombre varchar(255),
  descripcion varchar(255),
  precio number (10),
  CONSTRAINT fun_pk PRIMARY KEY (idFuncion)
);

CREATE SEQUENCE fun_seq;

CREATE OR REPLACE TRIGGER fun_bir 
BEFORE INSERT ON Funcion
FOR EACH ROW

BEGIN
  SELECT fun_seq.NEXTVAL
  INTO   :new.idFuncion
  FROM   dual;
END;

-------------------Venta-------------------------
create table Venta (
  idVenta number (10),
  nombre varchar(255),
  noTransaccion number,
  correo varchar(255),
  telefono number (10),
  tipo varchar(255),
  CONSTRAINT ven_pk PRIMARY KEY (idVenta)
);

CREATE SEQUENCE ven_seq;

CREATE OR REPLACE TRIGGER ven_bir 
BEFORE INSERT ON Venta
FOR EACH ROW

BEGIN
  SELECT ven_seq.NEXTVAL
  INTO   :new.idVenta
  FROM   dual;
END;

-----------------Venta_Detalle---------------
create table DetalleVenta (
  idVentaDetalle number (10),
  idAsiento number (10),
  idCartelera number (10),
  idFuncion number (10),
  CONSTRAINT asiento_fk FOREIGN KEY (idAsiento) References Asiento(idAsiento),
  CONSTRAINT cartelera_fk FOREIGN KEY (idCartelera) References Cartelera(idCartelera),
  CONSTRAINT funcion_fk FOREIGN KEY (idFuncion) References Funcion(idFuncion)
  
);

----------------INSERTS----------------------



-----------------------CLASIFICACION-------------------
INSERT INTO CLASIFICACION (NOMBRE,DESCRIPCION) VALUES ('TP','Todo Publico');
INSERT INTO CLASIFICACION (NOMBRE,DESCRIPCION) VALUES ('7','No recomendada para menores de 7 a�os');
INSERT INTO CLASIFICACION (NOMBRE,DESCRIPCION) VALUES ('10','No recomendada para menores de 10 a�os');
INSERT INTO CLASIFICACION (NOMBRE,DESCRIPCION) VALUES ('17','No recomendada para menores de 17 a�os');
INSERT INTO CLASIFICACION (NOMBRE,DESCRIPCION) VALUES ('18','No recomendada para menores de 18 a�os');

SELECT * FROM CLASIFICACION;

-----------------------PELICULA------------------------
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Exodus:dioses y reyes','Del aclamado director Ridley Scott llega Exodus: dioses y Reyes, una aventura epica que narra la historia de un hombre cuyo coraje desafio a un imperio. Mediante los efectos visuales y el 3D mas vanguardistas, Scott da nueva vida a la historia del desafiante lider Moises (Christian Bale) y su rebelion contra el faraón Ramses (Joel Edgerton), liberando a 600.000 esclavos en una epica huida de Egipto tras un terrorifico ciclo de mortiferas plagas.','http://www.youtube.com/embed/lE0KQ-Jcv0E','160 min','peliculas/imagenes/exodus.jpg','peliculas/imagenes/fondos/exodus.jpg',1);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Voces Inocentes','Cuenta la historia de Chava (Carlos Padilla), un niño de once años que atrapado por las circunstancias tiene que convertirse en «el hombre de la casa», después de que su padre los abandonara en plena Guerra Civil. Durante la década de los años ochenta en El Salvador, las fuerzas armadas del gobierno reclutaban niños de doce años sacándolos de sus escuelas. Si Chava tiene suerte, aún le queda un año de inocencia, un año antes de que él también sea enrolado y luche la batalla del gobierno contra los rebeldes del ejército FMLN (Frente Farabundo Martí para la Liberación Nacional).','http://www.youtube.com/embed/hRaMgKbb3Z4','105 min','peliculas/imagenes/voces.jpg','peliculas/imagenes/fondos/voces.jpg',5);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Cantinflas','Mike Todd, un excéntrico productor de Broadway, llega a Los �?ngeles con un proyecto de película bastante descabellado, La vuelta al mundo en 80 días, con el que quiere sacudir el star-system de Hollywood. Mario Moreno es un cómico que se gana la vida en las carpas de la Ciudad de México. Su personaje Cantinflas lo lleva a volverse un ícono del cine mexicano, y uno de los personajes más importantes de la industria fílmica.','http://www.youtube.com/embed/F-7Zg0l6NKk','120 min','peliculas/imagenes/cantinflas.jpg','peliculas/imagenes/fondos/cantinflas.jpg',1);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Titanic','Jack (DiCaprio), un joven artista, en una partida de cartas gana un pasaje para América, en el Titanic, el trasatlántico más grande y seguro jamás construido. A bordo, conoce a Rose (Kate Winslet), una joven de una buena familia venida a menos que va a contraer un matrimonio de conveniencia con Cal (Billy Zane), un millonario engreído a quien sólo interesa el prestigioso apellido de su prometida. Jack y Rose se enamoran, pero Cal y la madre de Rose ponen todo tipo de trabas a su relación. Inesperadamente, un inmenso iceberg pone en peligro la vida de los pasajeros..','http://www.youtube.com/embed/zCy5WQ9S4c0','200 min','peliculas/imagenes/titanic.jpg','peliculas/imagenes/fondos/titanic.jpg',3);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Buscando a nemo','La película cuenta el increíble periplo de dos peces - Marlin y su hijo Nemo - que se ven obligados a separarse en la Gran Barrera de Coral, ya que Nemo es capturado por un buceador. El pobre termina en la pecera de la consulta de un dentista desde la que se divisa el puerto de Sydney.','http://www.youtube.com/embed/X-Z-gQ8mKs0','110 min','peliculas/imagenes/nemo.jpg','peliculas/imagenes/fondos/nemo.jpg',1);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Titanes del pacifico','El aclamado cineasta Guillermo del Toro llega, de Warner Bros. Pictures y Legendary Pictures, llega �??Titanes Del Pacífico�.	Cuando las legiones de criaturas monstruosas, conocidas como Kaiju, comienzan a subir desde el mar, empieza una guerra que acabará con millones de vidas, consumiendo todos recursos de la humanidad durante años y años.','http://www.youtube.com/embed/4ekm20rSGs8','180 min','peliculas/imagenes/titanes.jpg','peliculas/imagenes/fondos/titanes.jpg',4);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Los ilusionistas','Un equipo del FBI intenta capturar a "Los Cuatro Jinetes", un grupo formado por los mejores magos del mundo, quienes se dedican a robar bancos mientras realizan sus habituales espectáculos, repartiendo luego el dinero que obtienen entre el público. Dylan, un agente especial, está empeñado en capturarlos antes de que lleven a cabo el que parece será su mayor golpe.','http://www.youtube.com/embed/EUWAvl9lUjQ','100 min','peliculas/imagenes/ilusionistas.jpg','peliculas/imagenes/fondos/ilusionistas.jpg',2);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Gigantes de acero','cuenta la historia de Charlie Kenton, un boxeador fracasado que pierde la posibilidad de ganar un título cuando poderosos robots de 90 kilos, y más de dos metros de altura, comienzan a competir. Convertido en un promotor de poca monta, Charlie sobrevive ensamblando robots de bajo costo, a los que apunta en peleas del circuito amateur.','http://www.youtube.com/embed/2G58gU25YdQ','130 min','peliculas/imagenes/gigantes.jpg','peliculas/imagenes/fondos/gigantes.jpg',4);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Transformers','Han pasado 4 años desde la tragedia de Chicago y la humanidad sigue reparando los destrozos, pero tanto los Autobots como los Decepticons han desaparecido de la faz de la Tierra. Ahora el Gobierno de los Estados Unidos está utilizando la tecnología rescatada en el asedio de Chicago para desarrollar sus propios Transformers.','http://www.youtube.com/embed/ragluX5mBzM','200 min','peliculas/imagenes/trans.jpg','peliculas/imagenes/fondos/transformers.jpg',1);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Avengers 2','Los estudios Marvel presentan Avengers: Age of Ultron, la continuación épica a la mayor película de superhéroes de todos los tiempos. Cuando Tony Stark trata de arrancar un programa suspendido para mantener la paz, las cosas se complican para los Héroes más poderosos de la Tierra. Iron Man, Capitán América, Thor, El increíble Hulk, la Viuda Negra y Hawkeye se enfrentan a la prueba definitiva mientras el destino del planeta pende de un hilo.','http://www.youtube.com/embed/LRTCFR4RBM0','130 min','peliculas/imagenes/avengers.jpg','peliculas/imagenes/fondos/avengers.jpg',1);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('El destino de Jupiter','Jupiter Jones naci� bajo el cielo de la noche, y las se�ales apuntaban a que estaba llamada a grandes cosas. Aunque sigue so�ando con las estrellas, su vida se reduce a la dura realidad de su trabajo como limpiadora de casas y a un sinf�n de rupturas problem�ticas. ','https://www.youtube.com/watch?v=Tb18zvrJm0k','100 min','peliculas/imagenes/jupiter.jpg','peliculas/imagenes/jupiter.jpg',1);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Peter Pan','En esta ocasi�n la historia sufrir� un vuelco importante, pues ser� Barbanegra quien capitanear� un barco volador que secuestra a los hu�rfanos de guerra, entre ellos Peter Pan (Miller), mientras que el Capit�n Garfio (Garrett Hedlund) es descrito como "un apuesto Indiana Jones".','https://www.youtube.com/watch?v=IVX9zqc-VWY','130 min','peliculas/imagenes/peter.jpg','peliculas/imagenes/peter.jpg',1);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Annie','Annie, interpretada por Quvenzhan� Wallis, es una joven lo suficientemente fuerte para abrirse paso en las calles de Nueva York en 2014. Annie fue dada en adopci�n cuando era un beb� con la promesa de que volver�an con ella alg�n d�a.','https://www.youtube.com/watch?v=RGofqY6oTGs','60 min','peliculas/imagenes/anni.jpg','peliculas/imagenes/anni.jpg',1);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Dos Tontos Todavia Mas Tontos','Han transcurrido exactamente veinte a�os desde que dejamos a Lloyd y a Harry jugando al escondite. En ese tiempo ha cambiado todo, excepto su supina idiotez.','https://www.youtube.com/watch?v=URGi6y91SdU','160 min','peliculas/imagenes/tontos.jpg','peliculas/imagenes/tontos.jpg',4);
INSERT INTO PELICULA (NOMBRE,DESCRIPCION,TRAILER,DURACION,imagen,imagen2,IDCLASIFICACION) VALUES ('Star wars','Se Desconoce','https://www.youtube.com/watch?v=et0djYt9LiA','100 min','peliculas/imagenes/starwars.jpg','peliculas/imagenes/starwars.jpg',2);

SELECT * FROM PELICULA;


-------------Complejo--------------

INSERT INTO COMPLEJO (NOMBRE,DIRECCION,CUIDAD,CALLCENTER,LATITUD,LONGITUD,MAPA) VALUES ('Palaza Atanasio','AV Petapa','Guatemala','92837465','14�3333.34','90�3256.39','https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.668771949262!2d-90.5468672!3d14.560924398096178!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8589a6cab9806c55%3A0xd3211ea5a06bfe42!2sCalz+Atanasio+Tzul%2C+Guatemala!5e0!3m2!1ses-419!2sgt!4v1423840549276');
INSERT INTO COMPLEJO (NOMBRE,DIRECCION,CUIDAD,CALLCENTER,LATITUD,LONGITUD,MAPA) VALUES ('Kinal','6Av 13-54','Guatemala','12344321','14�3731.89','90�3209.70','https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.537007336236!2d-90.53598299999997!3d14.625431000000006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8589a190308858f1%3A0x6795822b2b81d4ac!2sCentro+Educativo+Tecnico+Laboral+KINAL!5e0!3m2!1ses-419!2sgt!4v1423839692337') ;
INSERT INTO COMPLEJO (NOMBRE,DIRECCION,CUIDAD,CALLCENTER,LATITUD,LONGITUD,MAPA) VALUES ('Mira Flores','21 Av','Guatemala','67896789','14�3718.91','90�3310.21','https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3860.608225915662!2d-90.552543!3d14.621379999999998!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8589a1a514c48615%3A0xb116b3a6b9053306!2sGaler%C3%ADas+Miraflores!5e0!3m2!1ses-419!2sgt!4v1423840582460') ;
INSERT INTO COMPLEJO (NOMBRE,DIRECCION,CUIDAD,CALLCENTER,LATITUD,LONGITUD,MAPA) VALUES ('Carretera','Carretera al salvador','Guatemala','34565478','14�3718.91','90�3310.21','https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1931.1425169977974!2d-90.46710979648498!3d14.525681785546787!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8589a51f49171c4b%3A0x6b0dc8887f983427!2sCarr+a+El+Salvador%2C+Guatemala!5e0!3m2!1ses-419!2sgt!4v1424119788640') ;

SELECT * FROM COMPLEJO;
-------------Sala------------------
INSERT INTO SALA (NOMBRE,TIPO,IDCOMPLEJO) VALUES ('A','2D',1);
INSERT INTO SALA (NOMBRE,TIPO,IDCOMPLEJO) VALUES ('B','3D',1);
INSERT INTO SALA (NOMBRE,TIPO,IDCOMPLEJO) VALUES ('D','4D',1);
INSERT INTO SALA (NOMBRE,TIPO,IDCOMPLEJO) VALUES ('A','2D',2);
INSERT INTO SALA (NOMBRE,TIPO,IDCOMPLEJO) VALUES ('B','3D',2);
INSERT INTO SALA (NOMBRE,TIPO,IDCOMPLEJO) VALUES ('C','4D',2);
INSERT INTO SALA (NOMBRE,TIPO,IDCOMPLEJO) VALUES ('A','2D',3);
INSERT INTO SALA (NOMBRE,TIPO,IDCOMPLEJO) VALUES ('B','3D',3);
INSERT INTO SALA (NOMBRE,TIPO,IDCOMPLEJO) VALUES ('C','4D',3);

SELECT * FROM SALA;

-------------Asiento---------------

INSERT INTO ASIENTO (NOMBRE,IDASIENTO) VALUES ('',);

SELECT * FROM ASIENTO;

-------------Cartelera-------------
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('A','10:00','12:45','30-2-2015',1,1);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('B','11:00','12:50','29-2-2015',2,2);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('C','10:30','12:35','10-3-2015',3,3);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('D','13:00','16:30','11-3-2015',4,4);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('E','11:00','13:00','13-3-2015',5,5);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('F','10:00','13:05','15-3-2015',6,6);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('G','14:00','15:45','17-3-2015',7,7);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('H','15:00','17:20','27-3-2015',8,8);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('I','17:00','20:25','11-4-2015',9,9);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('J','11:00','13:20','15-4-2015',1,10);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('K','12:00','13:45','20-4-2015',2,11);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('L','15:00','17:20','10-5-2015',3,12);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('M','14:00','15:05','20-5-2015',4,13);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('N','11:00','13:45','30-5-2015',5,14);
INSERT INTO CARTELERA (NOMBRE,HORAINCIO,HORAFIN,FECHA,IDSALA,IDPELICULA) VALUES ('�','10:00','11:45','11-6-2015',6,15);

SELECT * FROM CARTELERA;
-------------Estreno---------------

INSERT INTO ESTRENO (FECHA,IDPELICULA) VALUES ('20-4-2015',11);
INSERT INTO ESTRENO (FECHA,IDPELICULA) VALUES ('10-5-2015',12);
INSERT INTO ESTRENO (FECHA,IDPELICULA) VALUES ('20-5-2015',13);
INSERT INTO ESTRENO (FECHA,IDPELICULA) VALUES ('30-5-2015',14);
INSERT INTO ESTRENO (FECHA,IDPELICULA) VALUES ('11-6-2015',15);

SELECT * FROM ESTRENO;

-------------Categoria-------------

INSERT INTO CATEGORIA (NOMBRE,DESCRIPCION) VALUES ('AC','Accion') ;
INSERT INTO CATEGORIA (NOMBRE,DESCRIPCION) VALUES ('R','Romantica') ;
INSERT INTO CATEGORIA (NOMBRE,DESCRIPCION) VALUES ('AN','Animada') ;
INSERT INTO CATEGORIA (NOMBRE,DESCRIPCION) VALUES ('CM','Comedia') ;

 
SELECT * FROM CATEGORIA;
-------------Pelicula_Categoria----

INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (1,1);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (2,1);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (3,4);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (4,2);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (5,3);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (6,1);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (7,1);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (8,1);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (9,1);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (10,1);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (11,1);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (12,4);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (13,4);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (14,4);
INSERT INTO PELICULA_CATEGORIA (IDPELICULA,IDCATEGORIA) VALUES (15,1);

SELECT * FROM PELICULA_CATEGORIA;

-------------Funcion---------------
INSERT INTO FUNCION (NOMBRE,DESCRIPCION,PRECIO) VALUES ();
INSERT INTO FUNCION (NOMBRE,DESCRIPCION,PRECIO) VALUES ();
INSERT INTO FUNCION (NOMBRE,DESCRIPCION,PRECIO) VALUES ();

SELECT * FROM FUNCION;


-------------Venta-----------------
SELECT * FROM VENTA;
-------------Venta_Detalle---------

SELECT * FROM DETALLEVENTA;